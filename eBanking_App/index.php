<?php

session_start();
require_once "./models/Benutzer.php";
require_once "./models/Konto.php";
require_once "./models/Transaktion.php";
require_once "./models/CookieHelper.php";



$loginError = '';

$cookieHelper = new CookieHelper();
if (isset($_POST['allow'])) {
    $cookieHelper->setAllowed();
}

$user = '';

if (isset($_POST['login'])) {
    $username = htmlspecialchars($_POST['username']);
    $password = htmlspecialchars($_POST['password']);

    $user = Benutzer::getByUsername($username);

    if ($user != null) {
        if ($user->getPasswort() == $password) {
            $user->login();
        } else {
            $loginError = "<div class='alert alert-danger'>Das Passwort ist falsch!</div>";
        }
    } else {
        $loginError = "<div class='alert alert-danger'>Es existiert kein Benutzer mit diesem Benutzernamen!</div>";
    }
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>e(asy)Banking</title>
</head>


<?php
if (!CookieHelper::isAllowed()) {
    ?>

    <body>
    <div class="container">

        <h1 class="col mt-5 mb-5 row justify-content-center">e(asy)-Banking</h1>

        <h2 class="mt-5 row justify-content-md-center">Willkommen </h2>
        <h5 class="mt-5 row justify-content-md-center">Diese Website verwendet Cookies. </h5>

        <form id='cookie_accept' method="post" action="index.php">

            <div class="row justify-content-md-center">
                <div class="form-group">
                    <input type="submit"
                           name="allow"
                           class="btn btn-warning mt-3"
                           value="Akzeptieren">
                </div>
            </div>
        </form>
    </div>
    </body>


    <?php
} else if (!Benutzer::isLoggedIn() && CookieHelper::isAllowed()) {
    ?>

    <body class="d-flex h-100 text-white bg-light">
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

        <header class="p-3 bg-dark text-white">
            <div class="container">
                <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">

                    <ul class="nav col-12 col-lg-auto me-lg-auto me-lg-auto justify-content-center mb-md-0">
                        <li><a href="index.php" class="nav-link px-2 text-white">Home</a></li>
                        <li><a href="views/layouts/about.php" class="nav-link px-2 text-white">Über uns</a></li>
                    </ul>
                    <div class="">
                        <a href="index.php" class="btn btn-outline-light me-2">Login</a>
                        <a href="./views/benutzer/create.php" class="btn btn-warning">Registrieren</a>
                    </div>
                </div>
            </div>
        </header>

        <main class="px-5 text-dark">
            <h1 class="mt-5 row justify-content-md-center">Willkommen bei e(asy)-Banking!</h1>


            <?= $loginError ?>

            <form class="mt-5" id="sign_up_form" method="post" action="index.php">

                <div class="row justify-content-md-center">
                    <div class="form-group col-4">
                        <label for="username">Benutzername*</label>
                        <input type="text"
                               name="username"
                               id="username"
                               class="form-control <?= isset($errors['username']) ? 'is-invalid' : '' ?>"
                               maxlength="25"
                               required>
                    </div>
                </div>

                <div class="row justify-content-md-center">
                    <div class="form-group col-4">
                        <label for="password">Passwort*</label>
                        <input type="password"
                               id="password"
                               name="password"
                               class="form-control <?= isset($errors['password']) ? "is-invalid" : "" ?>"
                               maxlength="25"
                               required="required">
                    </div>
                </div>

                <div class="row justify-content-md-center">
                    <div class="form-group">
                        <input type="submit"
                               name="login"
                               class="btn btn-outline-dark"
                               value="Login">
                    </div>
                    <div class="form-group ml-1">
                        <a href="index.php" class="btn btn-secondary btn-block">Leeren</a>
                    </div>
                </div>

            </form>

            <div class="text-center">
                <p class="mt-4 lead">Wenn Sie noch kein Benutzerkonto haben, bitte registrieren Sie sich.</p>
                <p class="lead">
                    <a href="views/benutzer/create.php" class="btn btn-warning">Registrieren</a>
                </p>

            </div>
        </main>
    </div>
    </body>

    <?php
} else {

    if (unserialize($_SESSION['user'])->getIstAngestellter()) {
        header("Location: ./views/konto/index.php");
    } else {
        header("Location: ./views/konto/view.php");
    }

}
?>

</html>

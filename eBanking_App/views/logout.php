<?php

session_start();

require_once "../models/Benutzer.php";


if (isset($_POST['logout'])) {
    Benutzer::logout();
    header("Location: ../index.php");
} else {
    http_response_code(405);
}
?>
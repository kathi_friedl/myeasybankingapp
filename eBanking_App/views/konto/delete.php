<?php
session_start();
$title = "Konto";
include '../layouts/top.php';
require_once "../../models/Benutzer.php";
require_once "../../models/Konto.php";
require_once "../../models/Transaktion.php";

if (unserialize($_SESSION['user'])->getIstAngestellter()) {

    if (isset($_POST['loeschen'])) {
        $kontoId = $_GET['id'];
        Konto::delete($kontoId);
        header("Location: ../../index.php");
    } else {
        http_response_code(405);
    }

} else {
    header("Location: ../../index.php");
}




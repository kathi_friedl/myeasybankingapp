<?php

session_start();

$title = "Kontenübersicht";
include '../layouts/top.php';
require_once "../../models/Benutzer.php";
require_once "../../models/Konto.php";
require_once "../../models/Transaktion.php";

if (Benutzer::isLoggedIn()){
    if (!unserialize($_SESSION['user'])->getIstAngestellter()) {
        header("Location: ../../index.php");
    }
}else{
    header("Location: ../../index.php");
}



?>

    <main class="px-5 text-dark">


        <h1 class="mt-5 mb-4 row justify-content-md-center"><?=$title?></h1>

        <hr>


        <?php
        if (Konto::getAll() != null) {
            ?>

            <table class="table table-striped mt-2">

                <tr>
                    <th>Kontoinhaber</th>
                    <th>IBAN</th>
                    <th>BIC</th>
                    <th>Kontostand</th>
                    <th></th>
                </tr>


                <?php
                foreach (Konto::getAll() as $k) {
                    $kontoinhaber = Benutzer::get($k->getVerfuegerId());
                    $name = $kontoinhaber->getVorname() . " " . $kontoinhaber->getNachname();
                    $iban = $k->getIban();
                    $bic = $k->getBic();
                    $kontostand = $k->getKontostand();

                    echo '<tr>';

                    echo "<td>$name</td>";
                    echo "<td>$iban</td>";
                    echo "<td>$bic</td>";
                    echo "<td>$kontostand €</td>";
                    ?>

                    <td>
                        <div class="row justify-content-md-center">
                            <form id='kontodetails' method="post" action="view.php?id=<?=$k->getId()?>">
                                <input type="hidden" name="id" value=<?= $k->getId() ?>>
                                <input type="submit" name="details" class="btn btn-outline-dark me-2" value="Details">

                            </form>
                            <form id='bartransaktion' method="post" action="../transaktion/createBar.php?id=<?=$k->getId()?>">
                                <input type="hidden" name="id" value=<?= $k->getId() ?>>
                                <input type="submit" name="bar" class="btn btn-secondary ml-1" value="Bar">

                            </form>
                            <form id='transaktion' method="post" action="../transaktion/create.php?id=<?=$k->getId()?>">
                                <input type="hidden" name="id" value=<?= $k->getId() ?>>
                                <input type="submit" name="transaktion" class="btn btn-warning ml-1" value="Transaktion">

                            </form>
                        </div>

                    </td>

                    <?php
                    echo '</tr>';
                }
                ?>


            </table>


            <?php
        } else {
            ?>

            <h4>Es sind noch keine Konten vorhanden!</h4>

            <?php
        }
        ?>

        <hr>


    </main>


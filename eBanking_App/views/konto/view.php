<?php
session_start();
$title = "Konto";
include '../layouts/top.php';
require_once "../../models/Benutzer.php";
require_once "../../models/Konto.php";
require_once "../../models/Transaktion.php";

if (!Benutzer::isLoggedIn()){
    header("Location: ../../index.php");
}

$konto = '';
if (!unserialize($_SESSION['user'])->getIstAngestellter()) {
    $user = (unserialize($_SESSION['user']));
    $konto = Konto::getKontoByVerfuegerId($user->getId());
} else {
    $konto = Konto::get($_GET['id']);
}

?>

<main class="px-5 text-dark">
    <h1 class="mt-5 mb-4 row justify-content-md-center"><?= $title ?></h1>

    <hr>
    <table class="table table-striped mt-2">

        <tr>
            <th>Kontoinhaber</th>
            <th>IBAN</th>
            <th>BIC</th>
            <th>Kontostand</th>
        </tr>

        <tr>
            <td><?= Benutzer::get($konto->getVerfuegerId())->getVorname()
                . " " . Benutzer::get($konto->getVerfuegerId())->getNachname() ?></td>
            <td><?= $konto->getIban() ?></td>
            <td><?= $konto->getBic() ?></td>
            <td><?= $konto->getKontostand() . " €" ?></td>
        </tr>

    </table>

    <hr>
    <div class="row mb-3">
        <form id='transaktion' method="post" action="../transaktion/create.php?id=<?= $konto->getId() ?>">
            <input type="submit" name="transaktion" class="btn btn-warning ml-3" value="Neue Transaktion">
        </form>

        <?php
        if (unserialize($_SESSION['user'])->getIstAngestellter()) {
            ?>
            <form id='loeschen' method="post" action="../konto/delete.php?id=<?= $konto->getId() ?>">
                <input type="submit" name="loeschen" class="btn btn-danger ml-1" value="Konto löschen">
            </form>
            <a href="index.php" class="btn btn-outline-dark ml-1">Zurück</a>
            <?php
        }
        ?>
    </div>



<div class="row mt-5">
    <h2 class="ml-3 mb-2 mr-3">Transaktionen</h2>


        <form id="search" action="view.php?id=<?= $konto->getId() ?>" method="post">
            <div class="row mt-1">
                <div class="col-sm-6">
                    <input type="text"
                           id="searchstring"
                           name="searchstring"
                           class="form-control">
                </div>
                <div>
                    <input type="submit"
                           id="search"
                           name="search"
                           class="btn btn-secondary"
                           value="Suchen">
                </div>
                <a href="../erweiterteSuche.php?id=<?=$konto->getId()?>" class="btn btn-outline-dark ml-1">Erweiterte Suche</a>
            </div>
        </form>
</div>


    <hr>
    <?php
    if (Transaktion::getAllTransaktionenByVerfuegerId($konto->getVerfuegerId()) != null) {
    ?>

    <table class="table table-striped mt-2 mb-4">
        <tr>
            <th>Datum</th>
            <th>Information</th>
            <th>Betrag</th>
            <th></th>

        </tr>

        <?php
        $gefundeneTransaktionen = [];
        if(isset ($_POST['search'])){
            $searchstring = strtolower(htmlspecialchars($_POST['searchstring']));
            foreach (Transaktion::getAll() as $transaktion) {
                if ($transaktion->getAbsenderId() == $konto->getVerfuegerId() || $transaktion->getEmpfaengerId() == $konto->getVerfuegerId()) {
                    $absendername= strtolower($transaktion->getAbsenderId() == 1 ? "Schalter" : Benutzer::get($transaktion->getAbsenderId())->getVorname() . " " . Benutzer::get($transaktion->getAbsenderId())->getNachname());
                    $empfaengername= strtolower($transaktion->getEmpfaengerId() == 1 ? "Schalter" : Benutzer::get($transaktion->getEmpfaengerId())->getVorname() . " " . Benutzer::get($transaktion->getEmpfaengerId())->getNachname());
                    $verwendungszweck = strtolower($transaktion->getVerwendungszweck());
                    $betrag = strtolower($transaktion->getBetrag());
                    $zeitpunkt = strtolower($transaktion->getZeitpunkt());

                    if(str_contains($absendername, $searchstring)
                        || str_contains($empfaengername, $searchstring)
                            || str_contains($verwendungszweck, $searchstring)
                                || str_contains($betrag, $searchstring)
                            || str_contains($zeitpunkt, $searchstring)){
                        $gefundeneTransaktionen[] = $transaktion;
                    }
                }
            }
        }
        ?>






        <?php
        $aufzulistendeTransaktionen = [];

        if(isset($_POST['search'])){
            $aufzulistendeTransaktionen = $gefundeneTransaktionen;
        }else{
            $aufzulistendeTransaktionen = Transaktion::getAll();
        }


        foreach ($aufzulistendeTransaktionen as $transaktion) {
            $id = $transaktion->getId();
            $absender = Benutzer::get($transaktion->getAbsenderId());
            $empfaenger = Benutzer::get($transaktion->getEmpfaengerId());
            if ($transaktion->getAbsenderId() == $konto->getVerfuegerId() || $transaktion->getEmpfaengerId() == $konto->getVerfuegerId()) {
                ?>

                <tr>
                    <td><?= $transaktion->getZeitpunkt() ?></td>

                    <?php
                    if ($transaktion->getAbsenderId() == $konto->getVerfuegerId()) {
                        ?>

                        <?php
                        if ($transaktion->getEmpfaengerId() == 1) {
                            ?>
                            <td><?= "Schalter" ?>
                                , <?= $transaktion->getVerwendungszweck() ?></td>
                            <?php
                        } else {
                            ?>
                            <td><?= $empfaenger->getVorname() . " " . $empfaenger->getNachname() ?>
                                , <?= $transaktion->getVerwendungszweck() ?></td>
                            <?php
                        }
                        ?>
                        <td>- <?= $transaktion->getBetrag() ?> €</td>
                        <?php
                    } else if ($transaktion->getEmpfaengerId() == $konto->getVerfuegerId()) {
                        ?>
                        <?php
                        if ($transaktion->getAbsenderId() == 1) {
                            ?>
                            <td><?= "Schalter" ?>
                                , <?= $transaktion->getVerwendungszweck() ?></td>
                            <?php
                        } else {
                            ?>
                            <td><?= $absender->getVorname() . " " . $absender->getNachname() ?>
                                , <?= $transaktion->getVerwendungszweck() ?></td>
                            <?php
                        }
                        ?>
                        <td>+ <?= $transaktion->getBetrag() ?> €</td>
                        <?php
                    }
                    ?>

                    <?php
                    if ($transaktion->getAbsenderId() == 1 || $transaktion->getEmpfaengerId() == 1) {
                        echo "<td><a href='../transaktion/viewBar.php?id=$id' class='btn btn-outline-dark ml-1'>Details</a></td>";
                    } else {
                        echo "<td><a href='../transaktion/view.php?id=$id' class='btn btn-outline-dark ml-1'>Details</a></td>";
                    }

                    ?>


                </tr>
                <?php
            }
        }
        } else {
            echo "Keine Transaktionen verfügbar!";
        }
        ?>

    </table>

    <hr>

</main>

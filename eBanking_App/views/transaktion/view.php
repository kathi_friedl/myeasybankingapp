<?php

session_start();
$title = "Konto";
include '../layouts/top.php';
require_once "../../models/Benutzer.php";
require_once "../../models/Konto.php";
require_once "../../models/Transaktion.php";

if (!Benutzer::isLoggedIn()){
    header("Location: ../../index.php");
}

$transaktion = Transaktion::get($_GET['id']);

?>


<main class="px-5 text-dark ml-5 mr-5">
    <h1 class="row justify-content-md-center mt-5 mb-5">Beleg</h1>


    <h5>Absenderdaten</h5>
    <table class="table table-striped mt-2">
        <tr>
            <th>Kontoinhaber</th>
            <th>IBAN</th>
            <th>BIC</th>

        </tr>

        <tr>
            <td><?= Benutzer::get($transaktion->getAbsenderId())->getVorname() . " " . Benutzer::get($transaktion->getAbsenderId())->getNachname() ?></td>
            <td><?=Konto::getKontoByVerfuegerId($transaktion->getAbsenderId())->getIban()?></td>
            <td><?=Konto::getKontoByVerfuegerId($transaktion->getAbsenderId())->getBic()?></td>
        </tr>
    </table>

    <hr>

    <h5 class="mt-4">Empfängerdaten</h5>
    <table class="table table-striped mt-2">

        <tr>
            <th>Kontoinhaber</th>
            <th>IBAN</th>
            <th>BIC</th>

        </tr>

        <tr>
            <td><?= Benutzer::get($transaktion->getEmpfaengerId())->getVorname() . " " . Benutzer::get($transaktion->getEmpfaengerId())->getNachname() ?></td>
            <td><?=Konto::getKontoByVerfuegerId($transaktion->getEmpfaengerId())->getIban()?></td>
            <td><?=Konto::getKontoByVerfuegerId($transaktion->getEmpfaengerId())->getBic()?></td>
        </tr>
    </table>

    <hr>


    <h5 class="mt-4">Transaktionsdetails</h5>
    <table class="table table-striped mt-2">
        <tr>
            <th>Betrag</th>
            <th>Zweck</th>
            <th>Zahlungsreferenz</th>
        </tr>
        <tr>
            <td><?= $transaktion->getBetrag() . " €" ?></td>
            <td><?= $transaktion->getVerwendungszweck() ?></td>
            <td><?= $transaktion->getZahlungsreferenz() ?></td>
        </tr>
    </table>

    <a href="../../index.php" class="btn btn-outline-dark ml-1">Zurück</a>
    <input type="button" class="btn btn-secondary ml-1" onclick="window.print();" value="Beleg drucken" />

</main>
</div>
</body>
</html>
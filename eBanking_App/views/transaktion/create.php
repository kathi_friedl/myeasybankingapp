<?php
session_start();

$title = "Überweisung";
include '../layouts/top.php';
require_once "../../models/Benutzer.php";
require_once "../../models/Konto.php";
require_once "../../models/Transaktion.php";

if (!Benutzer::isLoggedIn()) {
    header("Location: ../../index.php");
}

$konto = '';
if (!unserialize($_SESSION['user'])->getIstAngestellter()) {
    $user = (unserialize($_SESSION['user']));
    $konto = Konto::getKontoByVerfuegerId($user->getId());
} else {
    $konto = Konto::get($_GET['id']);
}
$message = '';

$transaktion = new Transaktion();

if (isset($_POST['transaction'])) {


    $empfaenger = Konto::getKontoByIBAN(htmlspecialchars($_POST['iban']));


    $transaktion->setAbsenderId($konto->getVerfuegerId());
    $empfaenger != null ? $transaktion->setEmpfaengerId(Konto::getKontoByIBAN(htmlspecialchars($_POST['iban']))->getVerfuegerId()) : $message = "<div class='alert alert-danger'>Ungültiger IBAN!</div>";
    $transaktion->setBetrag($_POST['amount']);
    $transaktion->setZahlungsreferenz(htmlspecialchars($_POST['reference']));
    $transaktion->setVerwendungszweck(htmlspecialchars($_POST['purpose']));

    $timestamp = time();
    $datum = date("Y-m-d", $timestamp);
    $uhrzeit = date("H:i", $timestamp);
    $zeitpunkt = $datum . " " . $uhrzeit;

    $transaktion->setZeitpunkt($zeitpunkt);

    if ($transaktion->getBetrag() < $konto->getKontostand()) {

        if ($transaktion->save()) {

            $absenderKonto = Konto::getKontoByVerfuegerId($transaktion->getAbsenderId());
            $empfaengerKonto = Konto::getKontoByVerfuegerId($transaktion->getEmpfaengerId());

            $absenderKonto->setKontostand($absenderKonto->getKontostand() - $transaktion->getBetrag());
            $absenderKonto->save();

            $empfaengerKonto->setKontostand($empfaengerKonto->getKontostand() + $transaktion->getBetrag());
            $empfaengerKonto->save();

            header("Location: view.php?id=" . $transaktion->getId());
            exit();

        }

    } else {
        $message = "<div class='alert alert-danger'>Kontostand zu gering!</div>";
    }
}

?>


<main class="px-5 text-dark ml-5 mr-5">


    <h1 class="row justify-content-md-center mt-5 mb-5"><?= $title ?></h1>

    <?= $message ?>
    <table class="table table-striped mt-2">

        <tr>
            <th>Kontoinhaber</th>
            <th>IBAN</th>
            <th>Kontostand</th>
        </tr>

        <tr>
            <td><?= Benutzer::get($konto->getVerfuegerId())->getVorname()
                . " " . Benutzer::get($konto->getVerfuegerId())->getNachname() ?></td>
            <td><?= $konto->getIban() ?></td>
            <td><?= $konto->getKontostand() . " €" ?></td>
        </tr>

    </table>

    <hr>


    <form class="mt-3" id="transaction_form" method="post" action="create.php?id=<?= $konto->getId() ?>">
        <h4>Empfängerdaten</h4>
        <div class="row justify-content-md-left">
            <div class="form-group col-4">
                <label for="iban">IBAN</label>
                <input type="text"
                       name="iban"
                       id="iban"
                       class="form-control"
                       minlength="12"
                       maxlength="12"
                       required>
                <?php if (!empty($transaktion->getErrors()['receiver'])): ?>
                    <div class="help-block text-danger"><?= $transaktion->getErrors()['receiver'] ?></div>
                <?php endif; ?>
            </div>

            <div class="form-group col-4">
                <label for="bic">BIC</label>
                <input type="text"
                       name="bic"
                       id="bic"
                       class="form-control"
                       minlength="8"
                       maxlength="8"
                       required>
            </div>


        </div>

        <hr>
        <h4>Überweisungsdetails</h4>


        <div class="row justify-content-md-left">
            <div class="form-group col-4">
                <label for="amount">Betrag</label>
                <input type="number"
                       step="0.01"
                       name="amount"
                       id="amount"
                       min="0.01"
                       class="form-control"
                       required>
                <?php if (!empty($transaktion->getErrors()['amount'])): ?>
                    <div class="help-block text-danger"><?= $transaktion->getErrors()['amount'] ?></div>
                <?php endif; ?>
            </div>

            <div class="form-group col-4">
                <label for="purpose">Überweisungszweck</label>
                <input type="text"
                       name="purpose"
                       id="purpose"
                       class="form-control"
                       maxlength="40"
                       required>
                <?php if (!empty($transaktion->getErrors()['purpose'])): ?>
                    <div class="help-block text-danger"><?= $transaktion->getErrors()['purpose'] ?></div>
                <?php endif; ?>
            </div>


            <div class="form-group col-4">
                <label for="reference">Zahlungsreferenz</label>
                <input type="text"
                       name="reference"
                       id="reference"
                       class="form-control"
                       maxlength="10"
                       required>
                <?php if (!empty($transaktion->getErrors()['reference'])): ?>
                    <div class="help-block text-danger"><?= $transaktion->getErrors()['reference'] ?></div>
                <?php endif; ?>
            </div>


        </div>


        <div class="row justify-content-md-left ml-2">
            <input type="submit"
                   name="transaction"
                   class="btn btn-warning mt-3"
                   value="Transaktion durchführen">

            <a href="create.php?id=<?= $konto->getId() ?>" class="btn btn-secondary col-1 mt-3 ml-2">Leeren</a>
            <a href="../../index.php" class="btn btn-outline-dark col-1 mt-3 ml-2">Zurück</a>
        </div>

    </form>
</main>
</div>

</body>
</html>

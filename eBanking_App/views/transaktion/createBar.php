<?php
session_start();
$title = "Barzahlung";
include '../layouts/top.php';
require_once "../../models/Benutzer.php";
require_once "../../models/Konto.php";
require_once "../../models/Transaktion.php";

if (!Benutzer::isLoggedIn()){
    header("Location: ../../index.php");
}


$konto = '';
if (!unserialize($_SESSION['user'])->getIstAngestellter()) {
    $user = (unserialize($_SESSION['user']));
    $konto = Konto::getKontoByVerfuegerId($user->getId());
} else {
    $konto = Konto::get($_GET['id']);
}

$message = '';

$transaktion = new Transaktion();

$zahlungsart ='';

if (isset($_POST['einzahlung']) || isset ($_POST['auszahlung'])) {


    if (isset($_POST['einzahlung'])) {
        $zahlungsart = 'Einzahlung';
        $transaktion->setAbsenderId(1);
        $transaktion->setEmpfaengerId($konto->getVerfuegerId());
    }

    if (isset($_POST['auszahlung'])) {
        $zahlungsart = 'Auszahlung';
        $transaktion->setAbsenderId($konto->getVerfuegerId());
        $transaktion->setEmpfaengerId(1);
    }

    $transaktion->setBetrag($_POST['amount']);
    $transaktion->setZahlungsreferenz(htmlspecialchars($_POST['reference']));
    $transaktion->setVerwendungszweck(htmlspecialchars($_POST['purpose']));


    $timestamp = time();
    $datum = date("Y-m-d", $timestamp);
    $uhrzeit = date("H:i", $timestamp);
    $zeitpunkt = $datum . " " . $uhrzeit;

    $transaktion->setZeitpunkt($zeitpunkt);

    if ($zahlungsart == "Auszahlung" && $transaktion->getBetrag() > $konto->getKontostand()) {
        $message = "<div class='alert alert-danger'>Kontostand zu gering!</div>";
    } else{
        if ($transaktion->save()) {

            if ($zahlungsart == "Einzahlung") {
                $konto->setKontostand($konto->getKontostand() + $transaktion->getBetrag());
                $konto->save();
            }

            if ($zahlungsart == "Auszahlung") {


                $konto->setKontostand($konto->getKontostand() - $transaktion->getBetrag());
                $konto->save();
            }

            header("Location: viewBar.php?id=" . $transaktion->getId());
            exit();
        }
}
}

?>

        <main class="px-5 text-dark ml-5 mr-5">
            <h1 class="row justify-content-md-center mt-5 mb-5"><?=$title?></h1>

            <?=$message?>
            <table class="table table-striped mt-2">

                <tr>
                    <th>Kontoinhaber</th>
                    <th>IBAN</th>
                    <th>Kontostand</th>
                </tr>

                <tr>
                    <td><?= Benutzer::get($konto->getVerfuegerId())->getVorname()
                        . " " . Benutzer::get($konto->getVerfuegerId())->getNachname() ?></td>
                    <td><?= $konto->getIban() ?></td>
                    <td><?= $konto->getKontostand() . " €" ?></td>
                </tr>

            </table>


            <hr>

            <a href="../../index.php" class="btn btn-outline-dark col-1 ml-2 mb-3">Zurück</a>


            <form class="mt-3" id="transaction_form" method="post" action="createBar.php?id=<?=$konto->getId()?>">

                <h4>Überweisungsdetails</h4>

                <div class="row justify-content-md-left">
                    <div class="form-group col-4">
                        <label for="amount">Betrag</label>
                        <input type="number"
                               step="0.01"
                               name="amount"
                               id="amount"
                               min="0.01"
                               class="form-control"
                               required>
                        <?php if (!empty($transaktion->getErrors()['amount'])): ?>
                            <div class="help-block"><?= $transaktion->getErrors()['amount'] ?></div>
                        <?php endif; ?>
                    </div>

                    <div class="form-group col-4">
                        <label for="purpose">Überweisungszweck</label>
                        <input type="text"
                               name="purpose"
                               id="purpose"
                               class="form-control"
                               maxlength="40"
                               required>
                        <?php if (!empty($transaktion->getErrors()['purpose'])): ?>
                            <div class="help-block"><?= $transaktion->getErrors()['purpose'] ?></div>
                        <?php endif; ?>
                    </div>


                    <div class="form-group col-4">
                        <label for="reference">Zahlungsreferenz</label>
                        <input type="text"
                               name="reference"
                               id="reference"
                               class="form-control"
                               maxlength="10"
                               required>
                        <?php if (!empty($transaktion->getErrors()['reference'])): ?>
                            <div class="help-block"><?= $transaktion->getErrors()['reference'] ?></div>
                        <?php endif; ?>
                    </div>


                </div>


                <div class="row justify-content-md-center ml-1">

                    <input type="submit" name="einzahlung" class="btn btn-success mt-3" value="+ Einzahlung +">

                    <a href="createBar.php?id=<?=$konto->getId()?>" class="btn btn-secondary col-1 mt-3 ml-2">Leeren</a>


                    <input type="submit" name="auszahlung" class="btn btn-danger mt-3 ml-2" value="- Auszahlung -">

                </div>

            </form>
        </main>
    </div>

    </body>
    </html>
<?php

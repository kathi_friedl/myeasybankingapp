<?php
require_once "../../models/Benutzer.php";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <title>e(asy)Banking</title>
</head>

<body class="d-flex h-100 text-white bg-light">
<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">


    <?php if(!Benutzer::isLoggedIn()){ ?>



    <header class="p-3 bg-dark text-white text-center ">
        <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">

                <ul class="nav col-12 col-lg-auto me-lg-auto me-lg-auto justify-content-center mb-md-0">
                    <li><a href="../../index.php" class="nav-link px-2 text-white">Home</a></li>
                    <li><a href="../layouts/about.php" class="nav-link px-2 text-white">Über uns</a></li>
                </ul>
                <div class="">
                    <a href="../../index.php" class="btn btn-outline-light me-2">Login</a>
                    <a href="../benutzer/create.php" class="btn btn-warning">Registrieren</a>
                </div>
            </div>
        </div>
    </header>


    <?php } else { ?>

        <header class="p-3 bg-dark text-white">
            <div class="container">
                <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">

                    <ul class="nav col-12 col-lg-auto me-lg-auto me-lg-auto justify-content-center mb-md-0">
                        <li><a href="../../index.php" class="nav-link px-2 text-white">Home</a></li>
                    </ul>

                    <form id='logout_form' method="post" action="../logout.php">
                        <div>
                            <input type="submit"
                                   name="logout"
                                   class="btn btn-warning"
                                   value="Logout">
                        </div>
                    </form>

                </div>
            </div>
        </header>

    <?php }?>


<?php
require_once "../../models/Konto.php";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <title>e(asy)Banking</title>
</head>

<body class="d-flex h-100 text-center text-white bg-light">

<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

    <header class="p-3 bg-dark text-white">
        <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">

                <ul class="nav col-12 col-lg-auto me-lg-auto me-lg-auto justify-content-center mb-md-0">
                    <li><a href="../../index.php" class="nav-link px-2 text-white">Home</a></li>
                    <li><a href="about.php" class="nav-link px-2 text-white">Über uns</a></li>
                </ul>

                <div class="">
                    <a href="../../index.php" class="btn btn-outline-light me-2">Login</a>
                    <a href="../benutzer/create.php" class="btn btn-warning">Registrieren</a>
                </div>

            </div>
        </div>
    </header>

    <main class="px-5 text-dark text-center">
        <h1 class="mt-5">Über e(asy)-Banking!</h1>
        <p class="mt-4 lead"> Mit uns können Sie Ihre Bankgeschäfte jederzeit online erledigen – mit einfachen,
            transparenten Produkten und neuesten Technologien.</p>

        <h4 class="mt-5">Unsere Bank gibt es seit:</h4>
        <p>12. November 2021</p>

        <h4 class="mt-5">Gründerin:</h4>
        <p>Katharina Friedl</p>


        <h4 class="mt-5">Aktuelle Kundenanzahl:</h4>
        <p>Aktuell sind <?=sizeof(Konto::getAll())?> Kunden bei uns registriert - und es werden immer mehr!</p>

        <h4 class="mt-5">Aktuelle Mitarbeiterzahl:</h4>
        <p>Wir haben bis jetzt eine Mitarbeiterin.</p>

<p> </p>
        <h5 class="mt-4">Bist du noch nicht registriert und würdest gerne Teil unserer Bankgesellschaft werden?</h5>
        <a href="../benutzer/create.php">Registriere dich HIER</a>



    </main>

</div>

</body>
</html>


<?php
session_start();
$title = "Erweiterte Suche";
require_once "../models/Benutzer.php";
require_once "../models/Konto.php";
require_once "../models/Transaktion.php";

if (!Benutzer::isLoggedIn()){
    header("Location: ../../index.php");
}

$konto = Konto::get($_GET['id']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <title>e(asy)Banking</title>
</head>

<body class="d-flex h-100 text-white bg-light">
<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

    <header class="p-3 bg-dark text-white">
        <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">

                <ul class="nav col-12 col-lg-auto me-lg-auto me-lg-auto justify-content-center mb-md-0">
                    <li><a href="../index.php" class="nav-link px-2 text-white">Home</a></li>
                </ul>

                <form id='logout_form' method="post" action="../logout.php">
                    <div>
                        <input type="submit"
                               name="logout"
                               class="btn btn-warning"
                               value="Logout">
                    </div>
                </form>

            </div>
        </div>
    </header>


    <main class="px-5 text-dark ml-5 mr-5">
        <h1 class="row justify-content-md-center mt-5 mb-5"><?= $title ?></h1>


        <h4>Suche nach Datum</h4>
        <hr>
        <form id="searchDate" action="erweiterteSuche.php?id=<?= $konto->getId() ?>" method="post">
            <div class="row">
                <label class="mt-2 ml-3" for="dateFrom">Datum von:</label>
                <div class="form-group col-3">
                    <input type="date"
                           name="dateFrom"
                           id="dateFrom"
                           class="form-control"
                           required>
                </div>

                <label class="mt-2 ml-4" for="dateTo">Datum bis:</label>
                <div class="form-group col-3">
                    <input type="date"
                           name="dateTo"
                           id="dateTo"
                           class="form-control"
                           required>
                </div>

                <div class="ml-2">
                    <input type="submit"
                           name="searchDate"
                           class="btn btn-secondary"
                           value="Suchen">
                </div>
            </div>
        </form>
        <hr>

        <h4>Suche nach Beträgen</h4>
        <hr>
        <form id="searchAmount" action="erweiterteSuche.php?id=<?= $konto->getId() ?>" method="post">
            <div class="row">
                <label class="mt-2 ml-3" for="amountFrom">Beträge von:</label>
                <div class="form-group col-3">
                    <input type="number"
                           name="amountFrom"
                           id="amountFrom"
                           class="form-control"
                           required>
                </div>

                <label class="mt-2 ml-4" for="amountTo">Beträge bis:</label>
                <div class="form-group col-3">
                    <input type="number"
                           name="amountTo"
                           id="amountTo"
                           class="form-control"
                           required>
                </div>

                <div class="ml-2">
                    <input type="submit"
                           name="searchAmount"
                           class="btn btn-secondary"
                           value="Suchen">
                </div>
            </div>
        </form>
        <hr>

        <h4>Suche nach Text</h4>

        <hr>
        <form id="searchTextString" action="erweiterteSuche.php?id=<?= $konto->getId() ?>" method="post">
            <div class="row">
                <label class="mt-2 ml-4" for="searchText">Suchtext:</label>
                <div class="form-group col-3">
                    <input type="text"
                           name="searchText"
                           id="searchText"
                           class="form-control"
                           required>
                </div>

                <div class="ml-2">
                    <input type="submit"
                           name="searchString"
                           class="btn btn-secondary"
                           value="Suchen">
                </div>
            </div>
        </form>
        <hr>
        <a href="../index.php" class="btn btn-outline-dark ml-1">Zurück</a>
        <hr>

        <?php
        if (!empty($_POST)) {
            ?>
            <table class="table table-striped mt-2 mb-4">
                <tr>
                    <th>Datum</th>
                    <th>Information</th>
                    <th>Betrag</th>
                    <th></th>

                </tr>


                <?php

                $gefundeneTransaktionen = [];

                foreach (Transaktion::getAll() as $transaktion) {
                    if ($transaktion->getAbsenderId() == $konto->getVerfuegerId() || $transaktion->getEmpfaengerId() == $konto->getVerfuegerId()) {

                        if (isset($_POST['searchDate'])) {
                            $dateTransaktion = date('Y-m-d', strtotime(substr($transaktion->getZeitpunkt(), 0, 10)));;
                            $dateFrom = date('Y-m-d', strtotime($_POST['dateFrom']));
                            $dateTo = date('Y-m-d', strtotime($_POST['dateTo']));

                            if ($dateTransaktion >= $dateFrom && $dateTransaktion <= $dateTo) {
                                $gefundeneTransaktionen[] = $transaktion;
                            }
                        } elseif (isset($_POST['searchAmount'])) {
                            if ($transaktion->getBetrag() >= $_POST['amountFrom'] && $transaktion->getBetrag() <= $_POST['amountTo']) {
                                $gefundeneTransaktionen[] = $transaktion;
                            }

                        } elseif (isset($_POST['searchString'])) {

                            $searchstring = strtolower($_POST['searchText']);

                            $absendername = strtolower($transaktion->getAbsenderId() == 1 ? "Schalter" : Benutzer::get($transaktion->getAbsenderId())->getVorname() . " " . Benutzer::get($transaktion->getAbsenderId())->getNachname());
                            $empfaengername = strtolower($transaktion->getEmpfaengerId() == 1 ? "Schalter" : Benutzer::get($transaktion->getEmpfaengerId())->getVorname() . " " . Benutzer::get($transaktion->getEmpfaengerId())->getNachname());
                            $verwendungszweck = strtolower($transaktion->getVerwendungszweck());

                            if (str_contains($absendername, $searchstring) || str_contains($empfaengername, $searchstring) || str_contains($verwendungszweck, $searchstring)) {
                                $gefundeneTransaktionen[] = $transaktion;
                            }

                        }

                    }
                }

                foreach ($gefundeneTransaktionen as $transaktion){

                    $id = $transaktion->getId();
                    $absender = Benutzer::get($transaktion->getAbsenderId());
                    $empfaenger = Benutzer::get($transaktion->getEmpfaengerId());

                        ?>
                        <tr>
                            <td><?= $transaktion->getZeitpunkt() ?></td>
                            <?php
                            if ($transaktion->getAbsenderId() == $konto->getVerfuegerId()) {
                                ?>

                                <?php
                                if ($transaktion->getEmpfaengerId() == 1) {
                                    ?>
                                    <td><?= "Schalter" ?>
                                        , <?= $transaktion->getVerwendungszweck() ?></td>
                                    <?php
                                } else {
                                    ?>
                                    <td><?= $empfaenger->getVorname() . " " . $empfaenger->getNachname() ?>
                                        , <?= $transaktion->getVerwendungszweck() ?></td>
                                    <?php
                                }
                                ?>
                                <td>- <?= $transaktion->getBetrag() ?> €</td>
                                <?php
                            } else if ($transaktion->getEmpfaengerId() == $konto->getVerfuegerId()) {
                                ?>
                                <?php
                                if ($transaktion->getAbsenderId() == 1) {
                                    ?>
                                    <td><?= "Schalter" ?>
                                        , <?= $transaktion->getVerwendungszweck() ?></td>
                                    <?php
                                } else {
                                    ?>
                                    <td><?= $absender->getVorname() . " " . $absender->getNachname() ?>
                                        , <?= $transaktion->getVerwendungszweck() ?></td>
                                    <?php
                                }
                                ?>
                                <td>+ <?= $transaktion->getBetrag() ?> €</td>
                                <?php
                            }
                            ?>

                            <?php
                            if ($transaktion->getAbsenderId() == 1 || $transaktion->getEmpfaengerId() == 1) {
                                echo "<td><a href='transaktion/viewBar.php?id=$id' class='btn btn-outline-dark ml-1'>Details</a></td>";
                            } else {
                                echo "<td><a href='transaktion/view.php?id=$id' class='btn btn-outline-dark ml-1'>Details</a></td>";
                            }

                            ?>


                        </tr>
                        <?php

                }

                ?>
            </table>
            <?php
        }
        ?>

    </main>
</div>

</body>
</html>

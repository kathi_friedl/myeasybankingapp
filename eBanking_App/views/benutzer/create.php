<?php

session_start();
$title = "Konto";
include '../layouts/top.php';
require_once "../../models/Benutzer.php";
require_once "../../models/Konto.php";
require_once "../../models/Transaktion.php";


$benutzer = new Benutzer();

//Serverseitige Validierung
if (isset($_POST["register"])) {

    $benutzer->setVorname($_POST['firstname']);
    $benutzer->setNachname($_POST['lastname']);
    $benutzer->setBenutzername($_POST['username']);
    $benutzer->setEmail($_POST['email']);
    $benutzer->setPasswort($_POST['password']);
    $benutzer->setIstAngestellter(0);

    if ($benutzer->save()) {
        $newKonto = new Konto();
        $newKonto->setVerfuegerId($benutzer->getId());
        $newKonto->setIban("AT" . rand(1000000000, 9999999999));

        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $var_size = strlen($chars);
        $bic = '';
        for ($x = 0; $x < 8; $x++) {
            $random_str = $chars[rand(0, $var_size - 1)];
            $bic .= $random_str;
        }

        $newKonto->setBic($bic);
        $newKonto->setKontostand(0.0);

        $newKonto->save();

        $benutzer->login();
        header("Location: ../../index.php");
    }
}
?>

<main class="px-5 text-dark ml-5 mr-5">
    <h1 class="row justify-content-md-center mt-5 mb-3">Registrieren</h1>


    <form class="mt-5" id="sign_up_form" method="post" action="create.php">

        <div class="row justify-content-md-center">
            <div class="form-group col-4">
                <label for="firstname">Vorname*</label>
                <input type="text"
                       name="firstname"
                       id="firstname"
                       class="form-control <?= isset($errors['firstname']) ? 'is-invalid' : '' ?>"
                       maxlength="25"
                       required>
                <?php if (!empty($benutzer->getErrors()['firstname'])): ?>
                    <div class="help-block"><?= $benutzer->getErrors()['firstname'] ?></div>
                <?php endif; ?>
            </div>
        </div>

        <div class="row justify-content-md-center">
            <div class="form-group col-4">
                <label for="lastname">Nachname*</label>
                <input type="text"
                       name="lastname"
                       id="lastname"
                       class="form-control <?= isset($errors['lastname']) ? 'is-invalid' : '' ?>"
                       maxlength="25"
                       required>
                <?php if (!empty($benutzer->getErrors()['lastname'])): ?>
                    <div class="help-block"><?= $benutzer->getErrors()['lastname'] ?></div>
                <?php endif; ?>
            </div>
        </div>

        <div class="row justify-content-md-center">
            <div class="form-group col-4">
                <label for="username">Benutzername*</label>
                <input type="text"
                       name="username"
                       id="username"
                       class="form-control <?= isset($errors['username']) ? 'is-invalid' : '' ?>"
                       maxlength="25"
                       required>
                <?php if (!empty($benutzer->getErrors()['username'])): ?>
                    <div class="help-block"><?= $benutzer->getErrors()['username'] ?></div>
                <?php endif; ?>
            </div>
        </div>

        <div class="row justify-content-md-center">
            <div class="form-group col-4">
                <label for="email">E-Mail*</label>
                <input type="email"
                       id="email"
                       name="email"
                       class="form-control <?= isset($errors['email']) ? 'is-invalid' : '' ?>"
                       required="required">
                <?php if (!empty($benutzer->getErrors()['email'])): ?>
                    <div class="help-block"><?= $benutzer->getErrors()['email'] ?></div>
                <?php endif; ?>
            </div>
        </div>

        <div class="row justify-content-md-center">
            <div class="form-group col-4">
                <label for="password">Passwort*</label>
                <input type="password"
                       id="password"
                       name="password"
                       class="form-control <?= isset($errors["password"]) ? "is-invalid" : "" ?>"
                       maxlength="25"
                       required="required">
                <?php if (!empty($benutzer->getErrors()['password'])): ?>
                    <div class="help-block"><?= $benutzer->getErrors()['password'] ?></div>
                <?php endif; ?>
            </div>
        </div>

        <div class="row justify-content-md-center">
            <input type="submit"
                   name="register"
                   class="btn btn-warning mt-3"
                   value="Registrieren">

            <a href="create.php" class="btn btn-secondary col-1 mt-3 ml-2">Leeren</a>

        </div>

    </form>
</main>
</div>

</body>
</html>

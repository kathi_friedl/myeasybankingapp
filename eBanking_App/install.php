<?php

require_once('models/Database.php');
require_once('models/Benutzer.php');
require_once('models/Konto.php');
require_once('models/Transaktion.php');

$db = Database::connect();

$sql = "CREATE TABLE IF NOT EXISTS `t_benutzer` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `vorname` varchar(255) NOT NULL,
  `nachname` varchar(255) NOT NULL,
  `benutzername` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `passwort` varchar(255) NOT NULL,
  `istAngestellter` tinyint(1) NOT NULL,
PRIMARY KEY (`id`), 
UNIQUE `email` (`email`), 
UNIQUE `benutzername` (`benutzername`)) 
ENGINE=InnoDB;";

$db->exec($sql);

$sql= "INSERT INTO `t_benutzer` (`id`, `vorname`, `nachname`, `benutzername`, `email`, `passwort`, `istAngestellter`) VALUES
(1, 'Miriam', 'Musterfrau', 'admin', 'mimuster@tsn.at', 'admin', 1),
(6, 'Werner', 'Bacher', 'webac', 'webac@tsn.at', 'userpw', 0),
(7, 'Ralf', 'Schuler', 'rasch', 'raschuler@tsn.at', 'userpw', 0),
(8, 'Herrmann', 'Fischer', 'hefisch', 'hefischer@tsn.at', 'userpw', 0),
(9, 'Katharina', 'Friedl', 'kafri', 'kafriedl@tsn.at', 'userpw', 0);";

$db->exec($sql);


$sql= "CREATE TABLE IF NOT EXISTS `t_konto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `verfueger_id` int(11) NOT NULL,
  `iban` varchar(255) NOT NULL,
  `bic` varchar(255) NOT NULL,
  `kontostand` double NOT NULL,
    PRIMARY KEY (`id`), UNIQUE `iban` (`iban`), UNIQUE `bic` (`bic`))
 ENGINE=InnoDB;";

$db->exec($sql);


$sql= "INSERT INTO `t_konto` (`id`, `verfueger_id`, `iban`, `bic`, `kontostand`) VALUES
(5, 6, 'AT5453933090', 'GLVADQLK', 98.48),
(6, 7, 'AT8063135801', 'ZAPNAMGV', 6.12),
(7, 8, 'AT5336673894', 'MIWUHZLV', 46.1),
(8, 9, 'AT7105291142', 'BNWIAJGX', 40.26);";

$db->exec($sql);



$sql = "CREATE TABLE IF NOT EXISTS `t_transaktion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `absender_id` int(11) NOT NULL,
  `empfaenger_id` int(11) NOT NULL,
  `betrag` double NOT NULL,
  `zahlungsreferenz` varchar(255) NOT NULL,
  `verwendungszweck` varchar(255) NOT NULL,
  `zeitpunkt` datetime NOT NULL,
    PRIMARY KEY (`id`)) 
    ENGINE=InnoDB;";

$db->exec($sql);

$sql="INSERT INTO `t_transaktion` (`id`, `absender_id`, `empfaenger_id`, `betrag`, `zahlungsreferenz`, `verwendungszweck`, `zeitpunkt`) VALUES
(11, 1, 6, 100.1, '1', 'Taschengeld', '2022-01-31 22:51:00'),
(12, 1, 8, 50.6, '2', 'Taschengeld', '2022-01-31 22:54:00'),
(13, 6, 7, 6.12, '3', 'Einkauf', '2022-01-31 23:17:00'),
(14, 8, 6, 4.5, '3', 'Mittagssnack', '2022-01-31 23:18:00'),
(15, 1, 9, 40.26, '5', 'Taschengeld', '2022-02-01 00:06:00');";

$db->exec($sql);

$sql = "ALTER TABLE `t_konto` 
    ADD CONSTRAINT `verfueger_id` 
        FOREIGN KEY (`verfueger_id`) 
            REFERENCES `t_benutzer`(`id`) 
            ON DELETE RESTRICT 
            ON UPDATE CASCADE;";

$db->exec($sql);

$sql = "ALTER TABLE `t_transaktion` 
    ADD CONSTRAINT `absender_id` 
        FOREIGN KEY (`absender_id`) 
            REFERENCES `t_benutzer`(`id`) 
            ON DELETE RESTRICT 
            ON UPDATE CASCADE;";

$db->exec($sql);

$sql = "ALTER TABLE `t_transaktion` 
    ADD CONSTRAINT `empfaenger_id` 
        FOREIGN KEY (`empfaenger_id`) 
            REFERENCES `t_benutzer`(`id`) 
            ON DELETE RESTRICT 
            ON UPDATE CASCADE;";

$db->exec($sql);

header('Location: index.php');
?>
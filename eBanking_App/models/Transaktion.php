<?php

require_once "DatabaseObject.php";

class Transaktion implements DatabaseObject
{

    private $id;
    private $absender_id;
    private $empfaenger_id;
    private $betrag;
    private $zahlungsreferenz;
    private $verwendungszweck;
    private $zeitpunkt;

    private $errors = [];

    // Konstruktor
    public function __construct()
    {

    }

    // Transaktion speichern
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                // known ID > 0 -> old object -> update
                $this->update();
            } else {
                // undefined ID -> new object -> create
                $this->id = $this->create();
            }

            return true;
        }

        return false;
    }


    // Serverseitige Validierung
    public function validate()
    {
        return $this->validateEmpfaengerId() & $this->validateBetrag() & $this->validateVerwendungszweck() & $this->validateZahlungsreferenz();
    }

    private function validateEmpfaengerId(){
        if($this->empfaenger_id == null){
            $this->errors["receiver"] = "Ungültiger IBAN!";
            return false;
        }
        return true;
    }

    private function validateBetrag()
    {
        if (!is_numeric($this->betrag)) {
            $this->errors["amount"] = "Der Betrag muss eine Zahl sein!";
            return false;
        } elseif ($this->betrag == 0) {
            $this->errors["amount"] = "Der Betrag darf nicht 0 sein!";
            return false;
        } else {
            return true;
        }
    }

    private function validateVerwendungszweck()
    {
        if (strlen($this->verwendungszweck) == 0) {
            $this->errors["purpose"] = "Der Verwendungszweck darf nicht leer sein!";
            return false;
        } elseif (strlen($this->verwendungszweck) > 40) {
            $this->errors["purpose"] = "Der Verwendungszweck ist zu lang!";
            return false;
        } else {
            return true;
        }
    }

    private function validateZahlungsreferenz()
    {
        if (strlen($this->zahlungsreferenz) == 0) {
            $this->errors["reference"] = "Die Zahlungsreferenz darf nicht leer sein!";
            return false;
        } elseif (strlen($this->zahlungsreferenz) > 10) {
            $this->errors["reference"] = "Die Zahlungsreferenz ist zu lang!";
            return false;
        } else {
            return true;
        }
    }


    // Getter
    public function getId()
    {
        return $this->id;
    }

    public function getAbsenderId()
    {
        return $this->absender_id;
    }

    public function getEmpfaengerId()
    {
        return $this->empfaenger_id;
    }

    public function getBetrag()
    {
        return $this->betrag;
    }

    public function getZahlungsreferenz()
    {
        return $this->zahlungsreferenz;
    }

    public function getVerwendungszweck()
    {
        return $this->verwendungszweck;
    }

    public function getZeitpunkt()
    {
        return $this->zeitpunkt;
    }

    public function getErrors()
    {
        return $this->errors;
    }




    // Setter
    public function setAbsenderId($absender_id)
    {
        $this->absender_id = $absender_id;
    }

    public function setEmpfaengerId($empfaenger_id)
    {
        $this->empfaenger_id = $empfaenger_id;
    }

    public function setBetrag($betrag)
    {
        $this->betrag = round($betrag, 2);
    }

    public function setZahlungsreferenz($zahlungsreferenz)
    {
        $this->zahlungsreferenz = $zahlungsreferenz;
    }

    public function setVerwendungszweck($verwendungszweck)
    {
        $this->verwendungszweck = $verwendungszweck;
    }

    public function setZeitpunkt($zeitpunkt)
    {
        $this->zeitpunkt = $zeitpunkt;
    }


    // Datenbankzugriff
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO t_transaktion (absender_id, empfaenger_id, betrag, zahlungsreferenz, verwendungszweck, zeitpunkt) values(?, ?, ?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->absender_id, $this->empfaenger_id, $this->betrag, $this->zahlungsreferenz, $this->verwendungszweck, $this->zeitpunkt));
        $lastId = $db->lastInsertId();  // get ID of new database-entry
        Database::disconnect();
        return $lastId;
    }

    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE t_transaktion set absender_id = ?, empfaenger_id = ?, betrag = ?, zahlungsreferenz = ?, verwendungszweck = ?, zeitpunkt WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->absender_id, $this->empfaenger_id, $this->betrag, $this->zahlungsreferenz, $this->verwendungszweck, $this->zeitpunkt, $this->id));
        Database::disconnect();
    }

    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM t_transaktion WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        // fetch dataset (row) per ID, convert to guest-object (ORM)
        $item = $stmt->fetchObject('Transaktion');
        Database::disconnect();

        return $item !== false ? $item : null;
    }

    public static function getAllTransaktionenByVerfuegerId($id)
    {
        $sql = "SELECT * FROM t_transaktion WHERE absender_id = ? OR empfaenger_id = ?";
        $db = Database::connect();
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id, $id));
        $item = $stmt->fetchObject('Transaktion');
        Database::disconnect();
        return $item !== false ? $item : null;
    }

    public static function getAll()
    {
        $db = Database::connect();
        $sql = 'SELECT * FROM t_transaktion';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        // fetch all datasets (rows), convert to array of guest-objects (ORM)
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Transaktion');
        Database::disconnect();

        return $items;

    }

    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM t_transaktion WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();

    }
}
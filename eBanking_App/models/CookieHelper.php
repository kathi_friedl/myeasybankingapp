<?php

class CookieHelper
{

    public function __construct()
    {
    }

    public static function isAllowed()
    {
        return isset($_SESSION['allowed']);
    }

    public function setAllowed()
    {
        $_SESSION['allowed'] = true;
    }
}
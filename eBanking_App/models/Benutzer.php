<?php

require_once "DatabaseObject.php";

class Benutzer implements DatabaseObject
{

    private $id;
    private $vorname;
    private $nachname;
    private $benutzername;
    private $email;
    private $passwort;
    private $istAngestellter;


    private $errors = [];


    // Konstruktor
    public function __construct()
    {

    }

    // Benutzer speichern
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                // known ID > 0 -> old object -> update
                $this->update();
            } else {
                // undefined ID -> new object -> create
                $this->id = $this->create();
            }

            return true;
        }

        return false;
    }


    // Methoden, die das Login und Logout eines Users betreffen
    public function login()
    {
        $u = serialize($this);
        $_SESSION['user'] = $u;
        return true;
    }

    public static function logout()
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }
    }

    public static function isLoggedIn()
    {
        if (isset($_SESSION['user'])) {
            return true;
        }
        return false;
    }


    // Validierung
    function validate()
    {
        return $this->validateVorname() &
            $this->validateNachname() &
            $this->validateBenutzername() &
            $this->validateEmail() &
            $this->validatePasswort();
    }


    private function validateVorname()
    {

        if (strlen($this->vorname) == 0) {
            $this->errors["firstname"] = "Der Vorname darf nicht leer sein!";
            return false;
        } else if (strlen($this->vorname) > 25) {
            $this->errors["firstname"] = "Der Vorname ist zu lang!";
            return false;
        } else {
            return true;
        }
    }

    private function validateNachname()
    {
        if (strlen($this->nachname) == 0) {
            $this->errors["lastname"] = "Der Nachname darf nicht leer sein!";
            return false;
        } else if (strlen($this->nachname) > 25) {
            $this->errors["lastname"] = "Der Nachname ist zu lang!";
            return false;
        } else {
            return true;
        }

    }

    private function validateBenutzername()
    {

        if (strlen($this->benutzername) == 0) {
            $this->errors["username"] = "Benutzername darf nicht leer sein!";
            return false;
        } else if (strlen($this->benutzername) > 25) {
            $this->errors["username"] = "Der Benutzername ist zu lang!";
            return false;
        } else {
            return true;
        }
    }

    private function validateEmail()
    {
        if ($this->email != "" && !filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->errors['email'] = "Format ungültig!";
            return false;
        } else if (strlen($this->email) < 5 || strlen($this->email) > 30) {
            $this->errors['email'] = "Länge der E-Mail ungültig!";
            return false;
        } else {
            return true;
        }
    }

    private function validatePasswort()
    {
        if (strlen($this->passwort) == 0) {
            $this->errors["password"] = "Passwort darf nicht leer sein!";
            return false;
        } else if (strlen($this->passwort) > 25) {
            $this->errors["password"] = "Das Passwort ist zu lang!";
            return false;
        } else {
            return true;
        }
    }


    // Getter
    public function getId()
    {
        return $this->id;
    }

    public function getVorname()
    {
        return $this->vorname;
    }

    public function getNachname()
    {
        return $this->nachname;
    }

    public function getBenutzername()
    {
        return $this->benutzername;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPasswort()
    {
        return $this->passwort;
    }

    public function getIstAngestellter()
    {
        if ($this->istAngestellter == 0){
            return false;
        }

        if($this->istAngestellter == 1) {
            return true;
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }


    //Setter
    public function setId($id)
    {
        $this->id = $id;
    }

    public function setVorname($vorname)
    {
        $this->vorname = $vorname;
    }

    public function setNachname($nachname)
    {
        $this->nachname = $nachname;
    }

    public function setBenutzername($benutzername)
    {
        $this->benutzername = $benutzername;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setPasswort($passwort)
    {
        $this->passwort = $passwort;
    }

    public function setIstAngestellter($istAngestellter)
    {
        $this->istAngestellter = $istAngestellter;
    }


    // Methode zum Prüfen, ob ein bestimmtes Feld Fehler aufweist
    public function hasError($field)
    {
        return isset($this->errors[$field]);
    }


    public function checkUsernameMitPasswort($benutzername, $passwort)
    {

        $doesExist = false;
        $correctPassword = false;

        foreach (self::getAll() as $u) {
            if ($u->getBenutzername() == $benutzername) {
                $doesExist = true;
                if ($u->getPasswort() == $passwort) {
                    $correctPassword = true;
                }
            }
        }

        if ($doesExist && $correctPassword) {
            return true;
        } else if (!$doesExist) {
            $this->errors['loginCheck'] = 'Es existiert kein Benutzer mit dem angegebenen Benutzername!';
            return false;
        } else if (!$correctPassword) {
            $this->errors['loginCheck'] = 'Das Passwort ist falsch!';
            return false;
        }
    }


    // Datenbankzugriff
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO t_benutzer (vorname, nachname, benutzername, email, passwort, istAngestellter) values(?, ?, ?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->vorname, $this->nachname, $this->benutzername, $this->email, $this->passwort, $this->istAngestellter));
        $lastId = $db->lastInsertId();  // get ID of new database-entry
        Database::disconnect();
        return $lastId;
    }

    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE t_benutzer set vorname = ?, nachname = ?, benutzername = ?, email = ?, passwort = ?, istAngestellter = ? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->vorname, $this->nachname, $this->benutzername, $this->email, $this->passwort, $this->istAngestellter, $this->id));
        Database::disconnect();
    }

    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM t_benutzer WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        // fetch dataset (row) per ID, convert to guest-object (ORM)
        $item = $stmt->fetchObject('Benutzer');
        Database::disconnect();

        return $item !== false ? $item : null;
    }

    public static function getByUsername($username)
    {
        $sql = "SELECT * FROM t_benutzer WHERE benutzername = ?";
        $db = Database::connect();
        $stmt = $db->prepare($sql);
        $stmt->execute(array($username));
        $item = $stmt->fetchObject('Benutzer');
        Database::disconnect();
        return $item !== false ? $item : null;
    }

    public static function getAll()
    {
        $db = Database::connect();
        $sql = 'SELECT * FROM t_benutzer';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        // fetch all datasets (rows), convert to array of guest-objects (ORM)
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Benutzer');
        Database::disconnect();

        return $items;
    }

    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM t_benutzer WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

}
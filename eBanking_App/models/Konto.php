<?php

require_once "DatabaseObject.php";

class Konto implements DatabaseObject
{

    private $id;
    private $verfueger_id;
    private $iban;
    private $bic;
    private $kontostand;

    private $errors = [];

    // Konstruktor
    public function __construct()
    {
    }

    // Konto speichern
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                // known ID > 0 -> old object -> update
                $this->update();
            } else {
                // undefined ID -> new object -> create
                $this->id = $this->create();
            }

            return true;
        }

        return false;
    }


    // Serverseitige Validierung
    public function validate()
    {
        return true;
    }


    // Getter
    public function getId()
    {
        return $this->id;
    }

    public function getVerfuegerId()
    {
        return $this->verfueger_id;
    }

    public function getIban()
    {
        return $this->iban;
    }

    public function getBic()
    {
        return $this->bic;
    }

    public function getKontostand()
    {
        return $this->kontostand;
    }

    public function getErrors()
    {
        return $this->errors;
    }


    // Setter
    public function setVerfuegerId($verfueger_id)
    {
        $this->verfueger_id = $verfueger_id;
    }

    public function setIban($iban)
    {
        $this->iban = $iban;
    }

    public function setBic($bic)
    {
        $this->bic = $bic;
    }

    public function setKontostand($kontostand)
    {
        $this->kontostand = round($kontostand, 2);
    }




    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO t_konto (verfueger_id, iban, bic, kontostand) values(?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->verfueger_id, $this->iban, $this->bic, $this->kontostand));
        $lastId = $db->lastInsertId();  // get ID of new database-entry
        Database::disconnect();
        return $lastId;
    }

    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE t_konto set verfueger_id = ?, iban = ?, bic = ?, kontostand = ? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->verfueger_id, $this->iban, $this->bic, $this->kontostand, $this->id));
        Database::disconnect();
    }

    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM t_konto WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        // fetch dataset (row) per ID, convert to guest-object (ORM)
        $item = $stmt->fetchObject('Konto');
        Database::disconnect();

        return $item !== false ? $item : null;
    }


    public static function getKontoByVerfuegerId($verfuegerId)
    {
        $sql = "SELECT * FROM t_konto WHERE verfueger_id = ?";
        $db = Database::connect();
        $stmt = $db->prepare($sql);
        $stmt->execute(array($verfuegerId));
        $item = $stmt->fetchObject('Konto');
        Database::disconnect();
        return $item !== false ? $item : null;
    }


    public static function getKontoByIBAN($iban)
    {
        $sql = "SELECT * FROM t_konto WHERE iban=?";
        $db = Database::connect();
        $stmt = $db->prepare($sql);
        $stmt->execute(array($iban));
        $item = $stmt->fetchObject('Konto');
        Database::disconnect();
        return $item !== false ? $item : null;
    }


    public static function getAll()
    {
        $db = Database::connect();
        $sql = 'SELECT * FROM t_konto';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        // fetch all datasets (rows), convert to array of guest-objects (ORM)
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Konto');
        Database::disconnect();

        return $items;

    }

    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM t_konto WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();

    }
}
# MyEasyBankingApp



As part of my PHP training, it was necessary to create an e-banking app using the server-side programming language PHP. This project contains all the content of the semester, concerning the PHP part.

The course content was:
- Form processing with appropriate validations
- Object-oriented programming with PHP
- Cookies and sessions
- Database access